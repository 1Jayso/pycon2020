## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- whats up
- wudup 

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- sure why not
- let's do it

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- nop

## intent:ask_course_availability
- I am interested in a course
- I am searching for a [programming](course_category) course
- Do you have [Gitlab](company) courses
- I would like to learn about [Python](course_topic)
- Do you have something about [devops](course_category)

## intent:ask_certificate
- do you offer certificates
- Will I get a certificate if I finish the course


## intent:ask_course_price
- and how much is it?
- how much does it cost?
- is it free?

## intent:ask_payment_method
- can I pay with card ?
- can I use my debit or credit card ?
- do you accept payPal
- can I pay with bitcoin??

## intent:ask_book_recommendation
- Best [book](learning_material) to learn [Java](course_topic)?
- please recommend a good book for my [algorithms](course_category) course
- any book recommendation for [Marketing](course_category) in IT?
- any cool book in [IT](course_category)
  

## intent: out_of_scope
- what are you made of?
- I need a refund