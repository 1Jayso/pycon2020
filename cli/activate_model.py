import argparse
import requests
import os 

# Create parser object
parser = argparse.ArgumentParser(description="Activate Trained Model")
parser.add_argument('model', action='store', help='Tag a model as production')
arguments = parser.parse_args()
print(f"the model name  is {arguments.model}")
header = {'Content-Type': 'application/json'}
body = {'username': 'me', 'password': os.environ['MODEL_SERVER_CREDENTIAL']}  # TO DO: Change the sensitive data
req = requests.post('http://34.89.139.156/api/auth', json=body, headers=header)
response = req.json()
bearer_token = response["access_token"]
project_id = 'default'
tag = 'production'
model = arguments.model

if bearer_token is not None:
    header = {'Authorization': "Bearer " + bearer_token,
              'Content-Type': 'application/json'}

    print("the URL string : ", f"http://34.89.139.156/api/projects/{project_id}/models/{model}/tags/{tag}", '\n')
    req = requests.put(f"http://34.89.139.156/api/projects/{project_id}/models/{model}/tags/{tag}", headers=header)
    print(req.text, type(req.text), len(req.text))
    if len(req.text) == 0:
        print(str(req.text), "the actual PUT Request")
        print("Model Activated")
else:
    print("Bearer token not retrieved")
