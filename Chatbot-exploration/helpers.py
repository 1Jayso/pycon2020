def get_conversations(server, bearer_token, limit=20):
    """
    This function will return the sender id, name, channel, intents, minimum action confidence detected among other metadata fields
    The response structure is [{several keys per id}, {several keys per id}]
    """
    server_endpoint = f"{server}/api/conversations?limit={limit}"
    header = {'Authorization': f"Bearer {bearer_token}"}
    try:
        conversations = requests.get(server_endpoint, headers=header).json()
        return conversations
    except:
        return None